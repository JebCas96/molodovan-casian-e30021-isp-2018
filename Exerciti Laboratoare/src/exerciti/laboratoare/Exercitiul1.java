package exerciti.laboratoare;

import java.util.Scanner;

public class Exercitiul1 {

    public static void main(String[] args) 
    {
        int n1,n2;
        
        Scanner citire = new Scanner(System.in);
        
        System.out.print("N1 = ");
        n1 = citire.nextInt();
        
        System.out.print("N2 = ");
        n2 = citire.nextInt();
        
        if(n1>n2)
        {
            System.out.println("Primul numar, (" + n1 + ") este mai mare decat al doilea (" + n2 + ").");
        }
        else if(n1 < n2)
        {
            System.out.println("Al doilea numar, (" + n2 + ") este mai mare decat primul (" + n1 + ").");
        }
        else
        {
            System.out.println("Cele doua numere sunt egale si au valoarea " + n1 + " .");
        }
    }        
}
    

