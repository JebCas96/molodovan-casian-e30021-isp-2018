package exerciti.laboratoare;

import java.util.Scanner;

public class Exercitiul3
{

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int A, B; //capetele listei
        boolean prim = true;
        int totalNrPrime = 0;

        System.out.print("Introduceit primul numar din lista (A) :");
        A = scan.nextInt();
        System.out.print("Introduceti ultimul numar al listei (B) :");
        B = scan.nextInt();

        System.out.print("Numerele prime sunt : ");
        for (int i = A; i <= B; i++)
        {
            prim = true;
            for (int j = 2; j < i / 2; j++)
            {
                if (i % j == 0)
                {
                    prim = false;
                }
            }
            if (prim == true && i != 1)
            {
                System.out.print(i + " ");
                totalNrPrime++;
            }
        }
        System.out.println(".");
        if (totalNrPrime == 0)
        {

            System.out.print("Nu exista numere prime.");
        }
        else
        {
            System.out.println("Sunt " + totalNrPrime + " numere prime in lista " + A + "-" + B + " .");
        }
    }
}
