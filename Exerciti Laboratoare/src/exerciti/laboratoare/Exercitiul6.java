package exerciti.laboratoare;

import java.util.Scanner;

public class Exercitiul6
{

    public static void main(String[] args)
    {
        int nr;
        Scanner citire = new Scanner(System.in);

        System.out.print("Introduceti un numar :");
        nr = citire.nextInt();

        System.out.println("Factorial de " + nr + " prin metoda recursiva este " + factorialRecursiv(nr) + " .");
        System.out.println("Iar prin metoda nerecursiva este tot " + factorialNerecursiv(nr) + " .");

    }

    public static int factorialRecursiv(int nr)
    {
        int rezultat;

        if (nr == 0 || nr == 1)
        {
            return 1;
        }

        rezultat = factorialRecursiv(nr - 1) * nr;

        return rezultat;
    }

    public static int factorialNerecursiv(int nr)
    {
        int rezultat = 1;

        for (int i = 1; i <= nr; i++)
        {
            rezultat = rezultat * i;
        }
        return rezultat;
    }

}
