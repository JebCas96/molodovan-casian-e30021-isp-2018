package exerciti.laboratoare;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Exercitiul7
{

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int dif = 1;
        int choice = 2;
        while (choice == 2)
        {
            loading();
            choice = scan.nextInt();
            switch (choice)
            {
                case 1:
                    choice = 1;
                    game(dif);
                    break;
                default:
                    System.out.println("###########################");
                    System.out.println("##^^^^^^^^^^^^^^^^^^^^^^^##");
                    System.out.println("##|      1.Easy   (6)   |##");
                    System.out.println("##|      2.Normal (9)   |##");
                    System.out.println("##|      3.Hard   (12)  |##");
                    System.out.println("##_______________________##");
                    System.out.println("###########################");
                    System.out.print("Enter the choice : ");
                    dif = scan.nextInt();
            }
        }
    }

    public static void game(int dif)
    {
        Scanner scan = new Scanner(System.in);
        int ans,fail=0;
        int ansOk = randomNR(dif);
        String b = "  ";

        switch (dif)
        {
            case 1:
                b = " 6";
                break;
            case 3:
                b = "12";
                break;
            default:
                b = " 9";
        }

        System.out.println("##############################");
        System.out.println("#  I'm thinking of a number  #");
        System.out.println("# between 1 and " + b + ",which is? #");
        System.out.println("##############################");
        System.out.print("Answer : ");

        for (int i = 0; i < 3; i++)
        {
            ans = scan.nextInt();
            if (ans == ansOk)
            {
                System.out.println("###########################");
                System.out.println("##^^^^^^^^^^^^^^^^^^^^^^^##");
                System.out.println("##|      You WON        |##");
                System.out.println("##|                     |##");
                System.out.println("###########################");                
                break;
            }
            else if (ans > ansOk)
            {
                System.out.println("###########################");
                System.out.println("##^^^^^^^^^^^^^^^^^^^^^^^##");
                System.out.println("##| Wrong answer, your  |##");
                System.out.println("##|  number it too high |##");
                System.out.println("###########################");
                System.out.print("Answer : ");
            }
            else if (ans < ansOk)
            {
                System.out.println("###########################");
                System.out.println("##^^^^^^^^^^^^^^^^^^^^^^^##");
                System.out.println("##| Wrong answer, your  |##");
                System.out.println("##|  number it too low  |##");
                System.out.println("###########################");
                System.out.print("Answer : ");
            }
            fail++;
        }
        if(fail == 3)
        {
        System.out.println("");
        System.out.println("###########################");
        System.out.println("##^^^^^^^^^^^^^^^^^^^^^^^##");
        System.out.println("##|      You LOST       |##");
        System.out.println("##|                     |##");
        System.out.println("###########################");
        System.out.println("Answer was " + ansOk);
        }
    }

    public static void loading()
    {
        try
        {
            System.out.println("###########################");
            System.out.println("##### Gues the number #####");
            System.out.println("###########################");
            System.out.print("#####");
            for (int i = 0; i < 17; i++)
            {
                TimeUnit.MILLISECONDS.sleep(300);
                System.out.print("=");
            }
            System.out.println("#####");
            System.out.println("###########################");
            System.out.println("##^^^^^^^^^^^^^^^^^^^^^^^##");
            System.out.println("##|        1.Start      |##");
            System.out.println("##|   2.Set Dificulty   |##");
            System.out.println("##_______________________##");
            System.out.println("###########################");
            System.out.print("Enter the choice : ");
        }
        catch (Exception e)
        {
        }
    }

    public static int randomNR(int dif)
    {
        Random rand = new Random();

        switch (dif)
        {
            case 3:
                return rand.nextInt(12) + 1;
            case 1:
                return rand.nextInt(6) + 1;
            default:
                return rand.nextInt(9) + 1;
        }
    }

}
