package lab4ex1;

public class Robot
{

    Engine robotEngine;

    public Robot()
    {
        robotEngine = new Engine();
    }

    public void moveRobot(int x, int y)
    {
        robotEngine.step(x, y);;
    }

}
