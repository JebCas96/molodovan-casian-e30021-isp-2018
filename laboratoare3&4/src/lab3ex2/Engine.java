package lab3ex2;

public class Engine
{

    String fuellType;
    long capacity;
    boolean active;
    int horsePower;

    Engine(int capacity, boolean active, int hp)
    {
        this.capacity = capacity;
        this.active = active;
        this.horsePower = hp;
    }

    Engine(int capacity, boolean active, String fuellType, int hp)
    {
        this(capacity, active, hp);
        this.fuellType = fuellType;
    }

    Engine()
    {
        this(2000, false, "diesel", 180);
    }

    void print()
    {
        System.out.println("Engine: capacity=" + this.capacity + " fuell=" + fuellType + " active=" + active);
    }

    public static void main(String[] args)
    {
        Engine tdi = new Engine();
        Engine i16 = new Engine(1600, false, "petrol", 130);
        Engine d30 = new Engine(3000, true, "diesel", 200);
        tdi.print();
        i16.print();
        d30.print();
    }
}
