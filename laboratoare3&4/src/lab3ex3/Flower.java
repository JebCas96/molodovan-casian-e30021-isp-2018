package lab3ex3;

import jdk.nashorn.internal.codegen.CompilerConstants;

public class Flower
{

    int petal;
    static int nrFlori = 0;

    Flower()
    {
        System.out.println("Flower has been created!");
        nrFlori++;
    }

    public static void afiseazaNrDeFlori()
    {
        System.out.println("Numarul de florii create este : " + nrFlori + " .");

    }

    public static void main(String[] args)
    {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++)
        {
            Flower f = new Flower();
            garden[i] = f;
        }
        afiseazaNrDeFlori();
    }
}
