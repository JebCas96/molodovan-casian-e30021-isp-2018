package lab4ex2;

import lab4ex1.*;

public class BattleField
{

    Robot r1;
    Robot r2;

    BattleField()
    {
        r1 = new Robot();
        r2 = new Robot();
    }

    public void play()
    {
        r1.moveRobot(15, 3, true);
        r2.moveRobot(-10, -5, false);
    }

    public static void main(String[] args)
    {
        BattleField game = new BattleField();
        game.play();
    }
}
