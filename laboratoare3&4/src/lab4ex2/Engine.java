package lab4ex2;

import lab4ex1.*;

class Engine
{

    void step(int x, int y)
    {
        start();
        checkDirection(x, y);
        execute(x, y);
        stop();
    }

    private void start()
    {
        System.out.println("Start engine.");
    }

    private void stop()
    {
        System.out.println("Stop engine.");
    }

    private void checkDirection(int x, int y)
    {
        if (x < 0)
        {
            System.out.println("Moving to the left.");
        }
        else if (x > 0)
        {
            System.out.println("Moving to the right.");
        }
        if (y < 0)
        {
            System.out.println("Moving backwards.");
        }
        else if (y > 0)
        {
            System.out.println("Moving forwards.");
        }
    }

    private void execute(int x, int y)
    {
        System.out.println("Moving (x)[" + Math.abs(x) + "]/(y)[" + Math.abs(y) + "] steps.");
    }

    void carryS()
    {
        System.out.println("An object is pickup.");
    }

    void carryF()
    {
        System.out.println("Relase the object.");
    }

}
