package lab4ex2;

import lab4ex1.*;

public class Robot
{

    Engine robotEngine;

    public Robot()
    {
        robotEngine = new Engine();
    }

    public void moveRobot(int x, int y, boolean carryObj)
    {

        if (carryObj == true)
        {
            robotEngine.carryS();
            robotEngine.step(x, y);
            robotEngine.carryF();
        }
        else
        {
            robotEngine.step(x, y);
        }
    }

}
